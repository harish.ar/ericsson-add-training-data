import fs from "fs/promises";

const chunksFilePath = 'chunks.json';

async function addTrainingDataToChunks(url, chunksFilePath, trainingDataObject) {
  try {
    // Use fs.promises.readFile for promise-based file reading
    const data = await fs.readFile(chunksFilePath, 'utf8');
    const chunks = JSON.parse(data);
    
    // Check if the specified URL exists in chunks and has chunks
    if (chunks[url]?.chunks?.length) {
      // console.log("chunks", chunks[url].chunks[0]);
      const firstChunk = chunks[url].chunks[0];
      // Check if training data exists for the URL
      if (trainingDataObject[url]?.length) {
        console.log("Inside training data");
        firstChunk.training_data = trainingDataObject[url];
      } else {
        firstChunk.training_data = [];
      }
      console.log("chunks", chunks);
      // Use fs.promises.writeFile for promise-based file writing
      try{
        await fs.writeFile(chunksFilePath, JSON.stringify(chunks, null, 2));
        // await fs.writeFile(chunksFilePath);
        console.log(`Successfully added training data to ${url}.`);
      }catch{
        console.log("Error writing training data");
      }
    } else {
      console.error(`Invalid chunks.json structure for ${url}. Expected an array with valid chunks.`);
    }
  } catch (err) {
    console.error(`Error processing ${url}:`, err);
  }
}

// Example usage
const trainingDataObject = {
  "https://eteamspace.internal.ericsson.com/display/JCAT/JCAT+R7+User+Guide": ["question1", "question2", "que3"],
  "https://eteamspace.internal.ericsson.com/display/JCAT/JCAT+R7+User+Guideabcdef": ["abc", "def", "ghi"],
  "https://eteamspace.internal.ericsson.com/display/JCAT/JCAT+R7+User+Guideabcdefg": ["a", "b", "c"]
};

// Use Promise.all to parallelize the async operations for each URL
const urls = Object.keys(trainingDataObject);
for(let i = 0; i < urls.length; i++) {
  await addTrainingDataToChunks(urls[i], chunksFilePath, trainingDataObject)
}
// urls.map(url => addTrainingDataToChunks(url, trainingDataObject));